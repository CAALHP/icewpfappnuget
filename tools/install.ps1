param($installPath, $toolsPath, $package, $project)

Write-host "Installing CareStore Wpf Application..."

$folder = $project.ProjectItems.Item("Images")
$file = $folder.ProjectItems.Item("app.png")

$copyToOutput = $file.Properties.Item("CopyToOutputDirectory")
$copyToOutput.Value = 1

$buildAction = $file.Properties.Item("BuildAction")
$buildAction.Value = 2